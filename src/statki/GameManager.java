package statki;

import java.util.Random;
import java.util.Scanner;


public class GameManager {

    private Random random = new Random();
    private int size;
    Scanner scanner = new Scanner(System.in);

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    private int sizeRandom() {
        int x;
        do {
            System.out.println("Podaj wielkosc planszy (więcej niż 5)");
            String fieldSize = scanner.nextLine();
            x = Integer.parseInt(fieldSize);
        } while (x < 5);
        return x;
    }

    public void checkCell(Player player, Field field) {
        do {
            System.out.printf("Podaj wspolrzedne dla X i Y lub wpisz quit aby zakonczyć);");
            String wspolrzedne = scanner.nextLine();
            String[] strings = wspolrzedne.trim().split(" ");
            if (wspolrzedne.toLowerCase().startsWith("quit")) {
                System.out.println("Quitting");
                break;
            }
            try {
                int x = Integer.parseInt(strings[0]);
                int y = Integer.parseInt(strings[1]);
                if (y <= getSize() && x <= getSize()) {
                    if (field.getField()[x][y].equals("O ")) {
                        System.out.println("Sprawdzam!");
                        field.getField()[x][y] = "X ";
                        break;
                    } else if (field.getField()[x][y].equals("S ")) {
                        System.out.println("Trafiony zatopiony!");
                        field.getField()[x][y] = "Z ";
                        addScore(player);
                        break;
                    } else {
                        System.out.println("Miejsce juz bylo sprawdzane!");
                    }
                }
            } catch (ArrayIndexOutOfBoundsException aiobe) {
                System.out.println("Wspolrzedne poza mapa");
            }
            printField(field);

        } while (true);
    }

    private void addScore(Player player) {
        player.setScore(player.getScore() + 1);
    }

    private int checkScore(Player player) {
        return player.getScore();
    }


    public void printField(Field field) {
        for (int i = 0; i < getSize(); i++) {
            System.out.println();
            for (int j = 0; j < getSize(); j++) {
                System.out.print(field.getField()[i][j]);
            }
        }
        System.out.println();
    }

    private void preparingField(Field f1, Field f2) {
        System.out.println("Gracz 1 rozstawia statki");
        settingShips(f1);
        System.out.println("Gracz 2 rozstawia statki");
        settingShips(f2);
    }

    private void settingShips(Field field) {
        int counter = 1;
        do {
            System.out.printf("Podaj wspolrzedne dla %d statka: ", counter);
            try {
                String wspolrzedne = scanner.nextLine();
                String[] strings = wspolrzedne.trim().split(" ");
                int x = Integer.parseInt(strings[0]);
                int y = Integer.parseInt(strings[1]);
                if (field.getField()[x][y].equals("S ")) {
                    System.out.println("W tym miejscu znajduje się juz statek. Podaj nowe wspolrzedne");
                } else {
                    field.getField()[x][y] = "S ";
                    counter++;
                }
            } catch (ArrayIndexOutOfBoundsException aiobe) {
                System.out.println("Wspolrzedne poza mapa");
            } catch (NumberFormatException nfe) {
                System.out.println("Nie wpisałeś nic!");
            }
        } while (counter != 6);
        System.out.println("Twoja mapa");
        printField(field);
    }

    public void run() {
        setSize(sizeRandom());

        Player player1 = new Player();
        Player player2 = new Player();
        Field field1 = new Field(getSize());
        Field field2 = new Field(getSize());
        preparingField(field1, field2);

        while (true) {
            System.out.println("Gracz 1");
            checkCell(player1, field2);
            if (checkScore(player1) == 5) {
                System.out.println("Gracz 1 wygrywa!");
                break;
            }
            System.out.println("Gracz 2");
            checkCell(player2, field1);
            if (checkScore(player2) == 5) {
                System.out.println("Gracz 2 wygrywa!");
                break;
            }
        }
    }
}
